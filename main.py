import json
import os
from datetime import datetime, timedelta

from dotenv import load_dotenv
from requests import get
from twilio.rest import Client

from helpers.helpers import (
    check_monday_or_thuesday_date,
    change_weekend_dates,
    format_message,
    get_dates_with_data_availability,
    get_stock_percentaje_movement,
    get_filtered_article,
)

load_dotenv()

ALPHA_VANTAGE = os.getenv('ALPHA_VANTAGE')
NEWS_API_TOKEN = os.getenv('NEWS_API_TOKEN')
TWILIO_SSID = os.getenv('TWILIO_SSID')
TWILIO_AUTH_TOKEN = os.getenv('TWILIO_AUTH_TOKEN')

STOCK = "TSLA"
COMPANY_NAME = "Tesla Inc"
TWILIO_PHONE_NUMBER = '+12183575599'
VERIFIED_CALL_NUMBER = '+51955152470'

ALPHA_URL = 'https://www.alphavantage.co/query'
NEWS_API_URL = 'https://newsapi.org/v2/everything'

message = ''

alpha_parameters = {
    'function': 'TIME_SERIES_DAILY',
    'symbol': STOCK,
    'apikey': ALPHA_VANTAGE
}


current_date = datetime.now().date()
# current_date = date(2021, 11, 29)

monthday_thuesday = check_monday_or_thuesday_date(current_date)
if monthday_thuesday:
    print(f'Disclaimer: The date {current_date} does not have the complete 2 days before data, so last week are taken.')

data_availability = change_weekend_dates(current_date)
yesterday_str = data_availability.get('yesterday')
before_yesterday_str = data_availability.get('before_yesterday')

# ALPHA_VANTAGE API CALL
tesla_stock_response = get(ALPHA_URL, alpha_parameters)
tesla_stock_response.raise_for_status

tesla_stock_data: json = tesla_stock_response.json()
history_stock_price = tesla_stock_data.get('Time Series (Daily)')

stock_yesterday = get_dates_with_data_availability(history_stock_price, yesterday_str)
stock_before_yesterday = get_dates_with_data_availability(history_stock_price, before_yesterday_str)

# STEP 1: Use https://www.alphavantage.co
# When STOCK price increase/decreases by 5% between yesterday and the day before yesterday then print("Get News").

percentaje_movement = get_stock_percentaje_movement(stock_yesterday, stock_before_yesterday)

# # STEP 2: Use https://newsapi.org
# Instead of printing ("Get News"), actually get the first 3 news pieces for the COMPANY_NAME.

print(percentaje_movement.get('rounded_percentage'))
if percentaje_movement.get('rounded_percentage') > 5.00:
    print('Get News')
else:
    print('Gaaa')

news_api_parameters = {
    'q': COMPANY_NAME,
    'from': current_date-timedelta(weeks=-1),
    'to': current_date,
    'sortBy': 'publishedAt',
    'apiKey': NEWS_API_TOKEN
}

news_api_response = get(NEWS_API_URL, news_api_parameters)
news_api_response.raise_for_status

news_api_data: json = news_api_response.json()
news_articles = news_api_data.get('articles')[0:3]

main_articles = [get_filtered_article(article) for article in news_articles]
# print(main_articles)

# # STEP 3: Use https://www.twilio.com
# Send a seperate message with the percentage change and each article's title and description to your phone number.

# TODO: CREATE A METHOD TO GET THE BODY OF THE MESSAGE WITH THE FORMAT
twilio_client = Client(TWILIO_SSID, TWILIO_AUTH_TOKEN)

for article in main_articles:
    message += format_message(
        percentaje_movement.get('status'),
        percentaje_movement.get('rounded_percentage'),
        article.get('title'),
        article.get('description')
        )

print(message)

result = twilio_client.messages.create(
        body=message,
        from_=TWILIO_PHONE_NUMBER,
        to=VERIFIED_CALL_NUMBER
)

print(result.status)

# Optional: Format the SMS message like this:
"""
TSLA: 🔺2%
Headline: Were Hedge Funds Right About Piling Into Tesla Inc. (TSLA)?.
Brief: We at Insider Monkey have gone over 821 13F filings that hedge funds and prominent investors are required to file by the SEC The 13F filings show the funds' and investors' portfolio positions as of March 31st, near the height of the coronavirus market crash.
or
"TSLA: 🔻5%
Headline: Were Hedge Funds Right About Piling Into Tesla Inc. (TSLA)?.
Brief: We at Insider Monkey have gone over 821 13F filings that hedge funds and prominent investors are required to file by the SEC The 13F filings show the funds' and investors' portfolio positions as of March 31st, near the height of the coronavirus market crash.
"""
