from datetime import datetime, timedelta
import html


def difference_date_to_str(date: datetime, day_quantity: int) -> str:
    '''Transforms the date obtained to a str type.'''
    day_quantity_timedelta = timedelta(days=day_quantity)

    date_type = date - day_quantity_timedelta
    date_str = str(date_type)

    return date_str


def transform_str_to_date(date_str: str):
    '''Transform a date in yyyy-mm-dd format to datetime'''
    to_format = "%Y-%m-%d"
    dt_object = datetime.strptime(date_str, to_format).date()
    return dt_object


def check_monday_or_thuesday_date(date_provided: datetime) -> bool:
    '''Verifies if the current date is monday or thuesday. Weekends does not generate stock records.'''
    if date_provided.weekday() <= 1:
        result = True
    else:
        result = False
    return result


def change_weekend_dates(day: datetime):
    '''Changes dates were monday and thuesday cases have weekend records empty.'''
    if day.weekday() == 0:
        yesterday = 3
        before_yesterday = 4
    elif day.weekday() == 1:
        yesterday = 1
        before_yesterday = 4
    else:
        yesterday = 1
        before_yesterday = 2

    data_availability = {
        'yesterday': difference_date_to_str(day, yesterday),
        'before_yesterday': difference_date_to_str(day, before_yesterday)
    }
    return data_availability


def get_dates_with_data_availability(stock_price_data: dict, stock_specific_day: str) -> dict:
    '''Checks if the date exists in the stock price data. If not takes the previous date that exists.'''
    verified_data: dict
    if stock_price_data.get(stock_specific_day) is not None:
        verified_data = stock_price_data.get(stock_specific_day)
    else:
        date_object = transform_str_to_date(stock_specific_day)
        new_date_str = difference_date_to_str(date_object, 1)
        verified_data = get_dates_with_data_availability(stock_price_data, new_date_str)
    return verified_data


def get_stock_percentaje_movement(stock_yesterday: dict, stock_before_y: dict) -> dict:
    ''' Returns a dictionary with the percentage. '''

    try:
        y_close = float(stock_yesterday.get('4. close'))
        by_close = float(stock_before_y.get('4. close'))

        if y_close > by_close:
            difference = y_close - by_close
            percentage = (difference*100)/y_close
            rounded_percentage = round(percentage, 2)
            result = {'status': 'increased', 'percentage': percentage, 'rounded_percentage': rounded_percentage}
        elif by_close > y_close:
            difference = by_close - y_close
            percentage = (difference*100)/by_close
            rounded_percentage = round(percentage, 2)
            result = {'status': 'decreased', 'percentage': percentage, 'rounded_percentage': rounded_percentage}
        else:
            percentage = 0.0
            result = {'status': 'maintained', 'percentage': percentage}

    except AttributeError:
        percentage = 0.0
        result = {'status': 'missing', 'percentage': percentage}

    return result


def get_filtered_article(article: dict) -> dict:
    ''' Gets the title and the description of an dictionary of articles.'''
    filtered_article = dict()

    filtered_article.setdefault('title', html.escape(article.get('title')))
    filtered_article.setdefault('description', html.escape(article.get('description')))
    return filtered_article


def format_message(status: str, percentage: float, title: str, description: str) -> str:
    '''Process a article to format a text message.'''
    message: str
    if status == 'increased':
        message = f'''
        TSLA: 🔺{percentage}%
        '''
    elif status == 'decreased':
        message = f'''
        TSLA: 🔻{percentage}%
        '''
    else:
        message = f'''
        TSLA: -{percentage}%
        '''

    message += f'''
    Headline: {title}
    Brief: {description}
    '''

    return message
